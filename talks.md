---
layout: page
permalink: /talks/
show: false
title: Talks
---

Algumas das talks que eu já dei no passado.

# Vídeos

Impulso talks Java:
- [It's alive! Dando vida ao monstro que valida e salva dados com o Prometheus moderno - Frankenstein!](https://www.youtube.com/live/ZdKsfiMUA3s?t=350&si=KS6pMWJUtEzH9qyk)
- [Implementando streams do Java 8 em TotalCross](https://www.youtube.com/live/ZdKsfiMUA3s?t=6243&si=A4OWvQ7S93rPzri7)

Sou Java na Campus Party:
- [Um parser em bash que identifica enums de um fonte Java](https://www.youtube.com/live/HKCz99cIiuE?si=67oARWCT6xbfqHmq)

# Slides

- [Docker 4 Engineers](https://jeffque.github.io/docker4engineers/#0)
- [Um parser em bash que identifica enums de um fonte Java](https://jeffque.github.io/bash-java-enums-parser-butwhyyy/)