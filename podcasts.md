---
layout: page
permalink: /podcasts/
show: false
title: Podcasts
---

# Todo mundo odeia Clean Code (Rock'n'Code)

Rock'n'Code com [@edgarberlinck](https://twitter.com/edgarberlinck), ep #30:

- [Spotify](https://open.spotify.com/show/52yZtjKJj3LFBIzh3eYAcT)
- [Anchor FM](https://podcasters.spotify.com/pod/show/curtinhasdoed/episodes/30---Todo-mundo-odeia-Clean-Code-feat-JeffQuesado-e-samsantosb-e26v03e)

Host:
- [Edgar Berlinck](https://twitter.com/edgarberlinck)

Convidados:
- Jefferson Quesado
- [Samuel Santos](https://twitter.com/samsantosb)

Canal:
- [Instagram](https://www.instagram.com/rockncodepod/)
- [Anchor FM](https://podcasters.spotify.com/pod/show/rockncode)
- [YouTube](https://youtube.com/@RocknCodePod)

# Embrace Legacy! (Carlos Nogueira, Engineering Sessions)

Engineering Sessions, S03E05:

- [YouTube](https://youtu.be/zOzLwJOe96w?feature=shared)

Host:
- [Carlos Nogueira](https://twitter.com/carlosenog)

Convidados:
- Jefferson Quesado
- [Lenadron Proença](https://twitter.com/leandronsp)
- [Rafael Ponte](https://twitter.com/rponte)

Canal:
- [YouTube](https://www.youtube.com/@carlosenog)